def solve():
    lines = get_lines()
    input = lines[0].strip()

    print(first_basement(input))
    

def final_floor(input):
    result = input.count("(") - input.count(")")
    print(input + ": " + str(result))
    return result


def first_basement(input):
    x = n = 0
    change = {'(' : 1, ')' : -1}
    for c in input:
        x += change[c]
        n += 1
        if x == -1:
            return n 

    return 0


def get_lines():
    filename = '01-data.txt'
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    return lines


if __name__ == '__main__':
    solve()
